export const state = () => ({
  products: [],
  cart: {
    user: {
      id: 1
    },
    overallPrice: 0,
    paymentType: "cash",
    meals: []
  },
  cartCount: 0,
  userInfo: {
    isLoggedIn: false,
    isSignedUp: false,
    hasSearched: false,
    name: "",
    productTitleSearched: ""
  },
  systemInfo: {
    openLoginModal: false,
    openSignupModal: false,
    openCheckoutModal: false
  }
});

export const getters = {
  productsAdded: (state) => {
    return state.cart.meals;
  },
  productsAddedToFavourite: (state) => {
    return state.products.filter((el) => {
      return el.isFavourite;
    });
  },
  getProductById: (state) => (id) => {
    return state.products.find((product) => product.id == id);
  },
  isUserLoggedIn: (state) => {
    return state.userInfo.isLoggedIn;
  },
  isUserSignedUp: (state) => {
    return state.userInfo.isSignedUp;
  },
  getUserName: (state) => {
    return state.userInfo.name;
  },
  isLoginModalOpen: (state) => {
    return state.systemInfo.openLoginModal;
  },
  isSignupModalOpen: (state) => {
    return state.systemInfo.openSignupModal;
  },
  isCheckoutModalOpen: (state) => {
    return state.systemInfo.openCheckoutModal;
  },
  quantity: (state) => {
    return state.products.quantity;
  },
  cartTotalPrice: (state) => {
    return (state.cart.overallPrice = state.cart.meals.reduce(
      (total, product) => {
        return total + product.price * product.quantity;
      },
      0
    ));
  }
};

export const mutations = {
  addToCart(state, item) {
    let found = state.cart.meals.find((product) => product.id == item.id);
    if (found) {
      found.quantity++;
      found.totalPrice = found.quantity * found.price;
    } else {
      state.cart.meals.push(item);
    }
    state.cart.overallPrice += item.price * item.quantity;

    console.log(state.cart.overallPrice);
    this.commit("saveCart", state.cart.meals);
    state.cartCount++;
  },
  removeFromCart(state, item) {
    let found = state.cart.meals.find((product) => product.id == item.id);
    if (found) {
      found.quantity--;
    } else {
      state.cart.meals.pop(item);
    }
    state.cart.overallPrice -= item.quantity * item.price;
    this.commit("saveCart", state.cart.meals);
    state.cartCount--;
  },
  fetchData(state, data) {
    state.products = data;
  },
  setUserData(state, data) {
    state.userInfo = data;
  },
  setSystemInfo(state, data) {
    state.systemInfo = data;
  },
  setAddedBtn: (state, data) => {
    state.products.forEach((el) => {
      if (data.id === el.id) {
        el.isAddedBtn = data.status;
      }
    });
  },
  removeProductsFromFavourite: (state) => {
    state.products.filter((el) => {
      el.isFavourite = false;
    });
  },
  isUserLoggedIn: (state, isUserLoggedIn) => {
    state.userInfo.isLoggedIn = isUserLoggedIn;
  },
  isUserSignedUp: (state, isSignedUp) => {
    state.userInfo.isSignedUp = isSignedUp;
  },
  setHasUserSearched: (state, hasSearched) => {
    state.userInfo.hasSearched = hasSearched;
  },
  setUserName: (state, name) => {
    state.userInfo.name = name;
  },
  setProductTitleSearched: (state, titleSearched) => {
    state.userInfo.productTitleSearched = titleSearched;
  },
  showLoginModal: (state, show) => {
    state.systemInfo.openLoginModal = show;
  },
  showSignupModal: (state, show) => {
    state.systemInfo.openSignupModal = show;
  },
  showCheckoutModal: (state, show) => {
    state.systemInfo.openCheckoutModal = show;
  },
  addToFavourite: (state, id) => {
    state.products.forEach((el) => {
      if (id === el.id) {
        el.isFavourite = true;
      }
    });
  },
  removeFromFavourite: (state, id) => {
    state.products.forEach((el) => {
      if (id === el.id) {
        el.isFavourite = false;
      }
    });
  },
  quantity: (state, data) => {
    state.products.forEach((el) => {
      if (data.id === el.id) {
        el.quantity = data.quantity;
      }
    });
  },
  SET_USER(state, authUser) {
    state.authUser = authUser;
  },
  saveCart(state) {
    window.localStorage.setItem("cart", JSON.stringify(state.cart));
    window.localStorage.setItem("cartCount", state.cartCount);
  }
};

/* export const action = {
  fetchData(store) {
    return fetch("./data.json").then((data) => {
      store.commit("fetchData", data);
    });
  }
};

  async nuxtServerInit({ commit }) {
    fetch("http://206.81.28.175:8080/api/meals",{
    "credentials": 'include',
    "headers": {
        "authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXBhaW9uNjkiLCJzY29wZXMiOnsiYXV0aG9yaXR5IjoiUk9MRV9VU0VSIn0sImV4cCI6MTU3MTE2MTIyN30.CFXj0uF-bRdrjKT5DpmOY_dT-c8aImtRh7aDWMrz2T7adG_WotmtuD54_S4DdzhIxt68_Iwf1LY5qmqWaBdqlg",
    },
    method: "GET",
    body: null,
    mode: "cors"
}).then(res=>res.json()).then(res=>{
    commit("fetchData", res);
})
  }
*/
export const actions = {
  async nuxtServerInit({ commit }) {
    const res = await (await fetch("http://localhost:4000/list")).json();
    commit("fetchData", res);
  },
  async userData({ commit }) {
    const res = await (await fetch("http://localhost:4000/userInfo")).json();
    commit("setUserData", res);
  },
  async systemInfo({ commit }) {
    const res = await (await fetch("http://localhost:4000/systemInfo")).json();
    commit("setSystemInfo", res);
  }
};
