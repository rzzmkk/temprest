[1mdiff --git a/store/data.json b/store/data.json[m
[1mindex 1f4e368..802dea9 100644[m
[1m--- a/store/data.json[m
[1m+++ b/store/data.json[m
[36m@@ -3,7 +3,7 @@[m
     {[m
       "id": 1,[m
       "title": "Sauerbraten",[m
[31m-      "price": "$10.99",[m
[32m+[m[32m      "price": 10.99,[m
       "url": "https://via.placeholder.com/600/92c952",[m
       "thumbnailUrl": "https://via.placeholder.com/150/92c952",[m
       "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",[m
[36m@@ -17,7 +17,7 @@[m
     {[m
       "id": 2,[m
       "title": "Jagerschnitzel",[m
[31m-      "price": "$15.99",[m
[32m+[m[32m      "price": 15.99,[m
       "url": "https://via.placeholder.com/600/92c952",[m
       "thumbnailUrl": "https://via.placeholder.com/150/92c952",[m
       "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",[m
[36m@@ -31,7 +31,7 @@[m
     {[m
       "id": 3,[m
       "title": "Double burger ",[m
[31m-      "price": "$9.99",[m
[32m+[m[32m      "price": 9.99,[m
       "url": "https://via.placeholder.com/600/92c952",[m
       "thumbnailUrl": "https://via.placeholder.com/150/92c952",[m
       "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",[m
[36m@@ -45,7 +45,7 @@[m
     {[m
       "id": 4,[m
       "title": "T-Bone hand-cut steak",[m
[31m-      "price": "$24.99",[m
[32m+[m[32m      "price": 24.99,[m
       "url": "https://via.placeholder.com/600/92c952",[m
       "thumbnailUrl": "https://via.placeholder.com/150/92c952",[m
       "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",[m
[36m@@ -59,7 +59,7 @@[m
     {[m
       "id": 5,[m
       "title": "Calamari",[m
[31m-      "price": "$8.99",[m
[32m+[m[32m      "price": 8.99,[m
       "url": "https://via.placeholder.com/600/92c952",[m
       "thumbnailUrl": "https://via.placeholder.com/150/92c952",[m
       "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",[m
[36m@@ -73,7 +73,7 @@[m
     {[m
       "id": 6,[m
       "title": "NY Buffalo Steak",[m
[31m-      "price": "$30.99",[m
[32m+[m[32m      "price": 30.99,[m
       "url": "https://via.placeholder.com/600/92c952",[m
       "thumbnailUrl": "https://via.placeholder.com/150/92c952",[m
       "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",[m
